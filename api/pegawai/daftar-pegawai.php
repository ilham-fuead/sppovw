<?php
include '../../../sppovw/vendor/autoload.php';
include '../../api/config/db_connection.php';

$pegawaiObj = json_decode(file_get_contents("php://input"));
$DBQueryObj = new DBQuery($host, $username, $password, $database_name);

//var_dump($pegawaiObj);exit;

$DBCmd1 = new DBCommand($DBQueryObj);

$DBCmd1->setINSERTintoTable('pegawai');
$DBCmd1->addINSERTcolumn('nama',$pegawaiObj->nama,IFieldType::STRING_TYPE);
$DBCmd1->addINSERTcolumn('gelaran',$pegawaiObj->gelaran,IFieldType::STRING_TYPE);
$DBCmd1->addINSERTcolumn('unit',$pegawaiObj->unit->id,IFieldType::STRING_TYPE);
$DBCmd1->executeQueryCommand();

//echo $DBCmd1->getSQLstring();exit;

if($DBCmd1->getAffectedRowCount()) {
    $obj = new MagicObject();
    $obj->dbstatus = 'success';
    $obj->msg = 'Butiran pegawai berjaya didaftarkan.';
}
else {
    $obj = new MagicObject();
    $obj->dbstatus = 'failed';
    $obj->msg = 'Butiran pegawai gagal didaftarkan.';
}
echo $obj->getJsonString();