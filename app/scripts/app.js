'use strict';

/**
 * @ngdoc overview
 * @name sppovwApp
 * @description
 * # sppovwApp
 *
 * Main module of the application.
 */
angular
  .module('sppovwApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($locationProvider,$routeProvider) {
    $locationProvider.hashPrefix('');
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/daftar-pegawai', {
        templateUrl: 'views/daftar-pegawai.html'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
