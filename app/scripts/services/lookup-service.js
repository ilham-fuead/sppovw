'use strict';

/**
 * @ngdoc service
 * @name sppovwApp.lookupService
 * @description
 * # lookupService
 * Factory in the sppovwApp.
 */
angular.module('sppovwApp')
  .factory('lookupService', ['$http', function ($http) {

      return {
          lookupUnit : function() {
              return $http.get('./api/pegawai/lookup-unit.php');
          }
      };
  }]);
