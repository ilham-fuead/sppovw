'use strict';

/**
 * @ngdoc function
 * @name sppovwApp.controller:DaftarPegawaiCtrl
 * @description
 * # DaftarPegawaiCtrl
 * Controller of the sppovwApp
 */
angular.module('sppovwApp')
  .controller('DaftarPegawaiCtrl', ['$log','pegawaiService','lookupService', function ($log,pegawaiService,lookupService) {
    var $ctrl = this;

      $ctrl.pegawai={};
      
      $ctrl.lookup_unit=[];

      $ctrl.pegawaiList=[];


      $ctrl.daftarPegawai = function () {

          $log.info('Pegawai',$ctrl.pegawai);

          pegawaiService.daftarPegawai($ctrl.pegawai).then(function (resp) {
              $log.info('resp',resp);
              alert(resp.data.msg);

              $ctrl.senaraiPegawai();

              //$ctrl.pegawai={};

          }, function (err) {
              $log.error('resp error',err);
          });


      };

      $ctrl.senaraiPegawai = function () {

          pegawaiService.senaraiPegawai().then(function (resp) {
              $log.info('resp senaraiPegawai',resp);

              $ctrl.pegawaiList=resp.data;
              //alert(resp.data.msg);

              //$ctrl.pegawai={};

          }, function (err) {
              $log.error('resp error senaraiPegawai',err);
          });

      };

      $ctrl.bindLookupUnit = function () {

          lookupService.lookupUnit().then(function (resp) {
              $log.info('resp lookupUnit',resp);

              $ctrl.lookup_unit=resp.data;
              //alert(resp.data.msg);

              //$ctrl.pegawai={};

          }, function (err) {
              $log.error('resp error lookupUnit',err);
          });

      };

      $ctrl.senaraiPegawai();
      $ctrl.bindLookupUnit();
      
  }]);
